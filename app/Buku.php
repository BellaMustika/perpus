<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'table_buku';

    protected $primaryKey = 'id_buku';

    protected $fillable = ['judul_buku','nama_donatur','deskripsi','kategori','cover_img'];
}