<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donatur;

class donaturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $donatur = Donatur::all();
        return view('donatur.index',compact('donatur'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('donatur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Donatur::create($request->all());
        return redirect('donatur')->with('msg','Data Berhasil di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $donatur = Donatur::find($id);
        return view('donatur.edit', compact('donatur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $edit = Donatur::find($request->input('id_donatur'));
        $edit->nama_donatur = $request->input('nama_donatur');
        $edit->alamat = $request->input('alamat');
        $edit->no_telp = $request->input('no_telp');
        $edit->save();

        return redirect('/donatur')->with('msg'.'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donatur = Donatur::find($id);
        $donatur->delete();

        return redirect('/donatur')->with('msg','donatur berhasil dihapus');
    }
}