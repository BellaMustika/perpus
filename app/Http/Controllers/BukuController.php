<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Buku;
use App\Donatur;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $donatur = Donatur::all();
        return view('buku.create',compact('donatur'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'judul_buku' => 'required',
            'cover_img' => 'required',
        ]);

        $file = $request->file('cover_img');
        $buku = new Buku;
        $buku->judul_buku = $request->judul_buku;
        $buku->nama_donatur   = $request->nama_donatur;
        $buku->kategori   = $request->kategori;
        $buku->cover_img = $file->getClientOriginalName();
        $tujuan_upload = 'image';
        $file->move($tujuan_upload,$file->getClientOriginalName());
        $buku->save();
        
        return redirect('/buku')->with('msg','Data Berhasil di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::where('id_buku',$id)->first();
        return $buku;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $donatur = Donatur::all();
        return view('buku.edit',['buku' => $buku,'donatur' => $donatur]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $file = $request->file('cover_img');
        $buku = Buku::find($request->input('id_buku'));
        $buku->judul_buku = $request->input('judul_buku');
        $buku->nama_donatur = $request->input('nama_donatur');
        $buku->kategori = $request->input('kategori');
        $buku->cover_img = $request->input('cover_img');
        $buku->cover_img = $file->getClientOriginalName();
        $tujuan_upload = 'image';
        $file->move($tujuan_upload,$file->getClientOriginalName());
        $buku->save();

        return redirect('buku')->with('msg','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Buku::find($id);
        $delete->delete();

        return redirect('buku')->with('msg','Data berhasil dihapus');
    }

}