<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donatur extends Model
{
    protected $table = 'table_donatur';

    protected $primaryKey = 'id_donatur';
    
    protected $fillable = ['nama_donatur','alamat','no_telp'];
}