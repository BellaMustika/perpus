@extends('layout.master')
@section('title', 'Laravel Perpustakaan')
@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<div class="card strpied-tabled-with-hover">
                                <div class="card-header ">
                                    <h4 class="card-title">Data Donatur</h4>
                                </div>
                                <div class="card-body table-full-width table-responsive">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr><th>ID</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>No. Telp</th>
                                        </tr></thead>
                                        <tbody>
                                        	@foreach($donatur as $don)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $don->nama_donatur}}</td>
                                                <td>{{ $don->alamat }}</td>
                                                <td>{{ $don->no_telp}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
<div class="card strpied-tabled-with-hover">
                                <div class="card-header ">
                                    <h4 class="card-title">Data Anggota</h4>
                                </div>
                                <div class="card-body table-full-width table-responsive">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                            	<th scope="col">ID</th>
                                            	<th scope="col">Nama</th>
                                            	<th scope="col">Alamat</th>
                                            	<th scope="col">Jenis Kelamin</th>
                                            	<th scope="col">No. Telp</th>
                                        	</tr>
                                    	</thead>
                                        <tbody>
                                        		@foreach($anggota as $ang)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $ang->nama_anggota}}</td>
                                                <td>{{ $ang->alamat}}</td>
                                                <td>{{ $ang->jenis_kelamin}}</td>
                                                <td>{{ $ang->no_telp}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
<div class="card strpied-tabled-with-hover">
                                <div class="card-header ">
                                    <h4 class="card-title">Data Buku</h4>
                                </div>
                                <div class="d-flex flex-row">	
                                @foreach($buku as $b)
                               <div class="card ml-3 mt-3 mr-3 mb-3" style="width: 18rem;">
								  <img src="image/{{ $b->cover_img }}" width="100px" height="300px" class="card-img-top">
								  <div class="card-body">
								    <h5 class="card-title">{{ $b->judul_buku}}</h5><br>
								    <h6 class="card-text">{{$b->kategori}}</h6>
								  </div>
								</div>
								@endforeach
                                </div>
                            </div>
<div class="card strpied-tabled-with-hover">
                                <div class="card-header ">
                                    <h4 class="card-title">Data Transaksi</h4>
                                </div>
                                <div class="card-body table-full-width table-responsive">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                            <th>ID</th>
                                            <th>ID Buku</th>
                                            <th>ID Peminjam</th>
                                            <th>Tgl Pinjam</th>
                                            <th>Tgl Kembali</th>
                                        	</tr>
                                        </thead>
                                        <tbody>
                                        	@foreach($transaksi as $trans)
                                            <tr>
                                                <td>{{ $trans->id}}</td>
                                                <td>{{$trans->id_buku}}</td>
                                                <td>{{$trans->id_anggota}}</td>
                                                <td>{{$trans->tgl_pinjam}}</td>
                                                <td>{{$trans->tgl_kembali}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
@endsection