@extends('layout.master')
@section('title', 'Laravel Perpustakaan')
@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-6">Edit Donatur Buku</h1>
        <hr class="my-4">
        <form action="/donatur/update" method="POST">
            @csrf
            <div class="form-group">
                <label for="deskripsi">Nama Donatur</label>
                <input type="text" class="form-control" id="deskripsi" name="nama_donatur" placeholder="Nama Donatur"
                    value="{{ $donatur->nama_donatur }}"><label for="deskripsi">Alamat</label>
                <input type="text" class="form-control" id="deskripsi" name="alamat" placeholder="Alamat"
                    value="{{ $donatur->alamat }}"><label for="deskripsi">No Telp</label>
                <input type="text" class="form-control" id="deskripsi" name="no_telp" placeholder="Nomor Telepon"
                    value="{{ $donatur->no_telp }}">
            </div>
            <input type="hidden" name="id_donatur" value="{{ $donatur->id_donatur}}">
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</div>
@endsection