@extends('layout.master')
@section('title', 'Laravel Perpustakaan')
@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-6">Tambah Donatur Buku</h1>
        <hr class="my-4">
        <form action="/donatur/store" method="POST">
            @csrf
            <div class="form-group">
                <label for="deskripsi">Nama Donatur</label>
                <input type="text" class="form-control" id="deskripsi" name="nama_donatur" placeholder="Nama Donatur"
                    value="{{ old('nama_donatur') }}"><label for="deskripsi">Alamat</label>
                <input type="text" class="form-control" id="deskripsi" name="alamat" placeholder="Alamat"
                    value="{{ old('alamat') }}"><label for="deskripsi">No Telp</label>
                <input type="text" class="form-control" id="deskripsi" name="no_telp" placeholder="Nomor Telepon"
                    value="{{ old('no_telp') }}">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</div>
@endsection