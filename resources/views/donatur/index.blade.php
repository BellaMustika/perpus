@extends('layout.master')
@section('title', 'Laravel Perpustakaan')
@section('content')
<div>
    <div class="jumbotron">
        @if(session('msg'))
        <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
            {{session('msg')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <h1 class="display-6">Donatur Buku</h1>
        <hr class="my-4">
        <a href="donatur/create" class="btn btn-primary mb-1">
            Tambah Donatur Buku</a>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Nama Donatur</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">No Telpon</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($donatur as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $d->nama_donatur }}</td>
                    <td>{{ $d->alamat }}</td>
                    <td>{{ $d->no_telp }}</td>
                    <td>
                        <a href="/donatur/edit/{{ $d->id_donatur }}" class="badge badge-primary">Edit</a>
                        <a href="/donatur/delete/{{ $d->id_donatur }}" class="badge badge-danger" onclick="return confirm('Yakin hapus data {{$d->nama_donatur}}?')">Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection