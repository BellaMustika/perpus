@extends('layout.master')
@section('title', 'Laravel - Perpustakaan')
@section('content')
<div>
    <div class="jumbotron">
        @if(session('msg'))
        <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
            {{session('msg')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <h1 class="display-6">Data Buku</h1>
        <hr class="my-4">
        <a href="buku/create" class="btn btn-primary mb-1">Tambah Buku</a>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Judul Buku</th>
                    <th scope="col">Nama Donatur</th>
                    <th scope="col">Kategori Buku</th>
                    <th scope="col">Cover Buku</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($buku as $b)
                <tr>
                    <td>{{ $b->id_buku }}</td>
                    <td>{{ $b->judul_buku }}</td>
                    <td>{{ $b->nama_donatur }}</td>
                    <td>{{ $b->kategori }}</td>
                    <td><img src='image/{{ $b->cover_img }}' style='width:80px; height:50px;'></td>
                    <td>
                        <a href="{{url('/buku/edit/'.$b->id_buku)}}" class="badge badge-primary">Edit</a>
                        <a href="{{url('/buku/delete/'.$b->id_buku)}}" class="badge badge-danger" onclick="return confirm('Yakin hapus buku {{ $b->judul_buku }}?')">Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection