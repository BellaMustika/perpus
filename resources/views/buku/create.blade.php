@extends('layout.master')
@section('title', 'Laravel - Perpustakaan')
@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-6">Tambah Data Buku</h1>
        <hr class="my-4">
        <form action="/buku/store" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="judul_buku">Judul Buku</label>
                <input type="text" class="form-control" name="judul_buku" placeholder="Judul Buku"
                    value="{{ old('judul_buku') }}">
            </div>
            <div class="form-group">
                <label for="deskripsi">Nama Donatur</label>
                <select class="form-control form-control-sm" id="nama_donatur" name="nama_donatur">
                    @foreach($donatur as $donat)
                    <option value="{{ $donat->nama_donatur }}">{{ $donat->nama_donatur }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="deskripsi">Kategori</label>
                <input type="text" class="form-control" name="kategori" placeholder="Kategori Buku"
                    value="{{ old('kategori') }}">
            </div>
            <div class="form-group">
                <label for="cover_img">Cover Buku</label>
                <input type="file" name="cover_img">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</div>
@endsection