@extends('layout.master')
@section('title', 'Laravel - Perpustakaan')
@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-6">Edit Data Buku</h1>
        <hr class="my-4">
        <form action="{{url('/buku/update')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="judul_buku">Judul Buku</label>
                <input type="text" class="form-control" name="judul_buku" placeholder="Judul Buku"
                    value="{{ $buku->judul_buku }}">
            </div>
            <div class="form-group">
                <label for="deskripsi">Kategori Buku</label>
                <input type="text" class="form-control" name="kategori" placeholder="Kategori Buku"
                    value="{{ $buku->kategori }}">
            </div>
            <div class="form-group">
                <label for="deskripsi">Nama Donatur</label>
                <input type="text" class="form-control" name="nama_donatur" placeholder="Nama Donatur"
                    value="{{ $buku->nama_donatur }}">
            </div>
            <div class="form-group">
                <label for="cover_img">Cover Buku</label>
                <input type="file" name="cover_img"><br>
                <label>Cover tersimpan : {{ $buku->cover_img}}</label>
            </div>
            <input type="hidden" name="id_buku" value="{{ $buku->id_buku }}">
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</div>
@endsection