@extends('layout.master')
@section('title', 'Laravel Perpustakaan')
@section('content')
<div>
    <div class="jumbotron">
        @if(session('msg'))
        <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
            {{session('msg')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <h1 class="display-6">Data Peminjaman Buku</h1>
        <hr class="my-4">
        <a href="transaksi/create" class="btn btn-primary mb-1">
            Peminjaman Buku</a>
            <button type="button" class="btn btn-outline-primary float-right">
                Export data
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-file-earmark-spreadsheet" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M5 10H3V9h10v1h-3v2h3v1h-3v2H9v-2H6v2H5v-2H3v-1h2v-2zm1 0v2h3v-2H6z"></path>
                    <path d="M4 0h5.5v1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h1V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z"></path>
                    <path d="M9.5 3V0L14 4.5h-3A1.5 1.5 0 0 1 9.5 3z"></path>
                </svg>
        </button>
        <!-- <a href="buku/kembali" class="btn btn-primary mb-1">Pengembalian Buku</a>        -->
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No. Peminjaman</th>
                    <th scope="col">Nama Peminjam</th>
                    <th scope="col">Judul Buku</th>
                    <th scope="col">Tanggal Pinjam</th>
                    <th scope="col">Tanggal Kembali</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transaksi as $trans)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $trans->nama_anggota }}</td>
                    <td>{{ $trans->judul_buku }}</td>
                    <td>{{ $trans->tgl_pinjam }}</td>
                    <td>{{ $trans->tgl_kembali }}</td>
                    <td>
                        @if($trans->tgl_kembali == null)
                        <a href="transaksi/edit/{{ $trans->id }}" class="badge badge-primary">Pengembalian</a>
                        @else
                        <p class="badge badge-success">Dikembalikan</p>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection