<?php

use App\Http\Controllers\BukuController;
use App\Http\Controllers\KategoriController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@index');

Route::get('/master', function () {
    return view('layout.master');
});

Route::resource('transaksi','TransaksiController');
Route::post('/transaksi/store','TransaksiController@store');
Route::get('transaksi/edit/{id}', 'TransaksiController@edit');
Route::get('transaksi/showBuku/{id}', 'TransaksiController@showBuku');
Route::get('transaksi/getAnggota/{id}', 'TransaksiController@getAnggota');
Route::post('/transaksi/update/{id}', 'TransaksiController@update');

Route::get('/anggota','AnggotaController@index');
Route::get('/anggota/create','AnggotaController@create');
Route::post('/anggota/store','AnggotaController@store');
Route::get('/anggota/edit/{id}','AnggotaController@edit');
Route::post('/anggota/update','AnggotaController@update');
Route::get('/anggota/delete/{id}','AnggotaController@destroy');

Route::get('/donatur','DonaturController@index');
Route::get('/donatur/create','DonaturController@create');
Route::post('/donatur/store','DonaturController@store');
Route::get('/donatur/edit/{id}','DonaturController@edit');
Route::post('/donatur/update','DonaturController@update');
Route::get('/donatur/delete/{id}','DonaturController@destroy');

Route::get('/buku','BukuController@index');
Route::get('/buku/create','BukuController@create');
Route::post('/buku/store','BukuController@store');
Route::get('/buku/edit/{id}','BukuController@edit');
Route::post('/buku/update','BukuController@update');
Route::get('/buku/delete/{id}','BukuController@destroy');

Route::get('/transaksi','TransaksiController@index');
Route::get('/transaksi/create','TransaksiController@create');
Route::post('/transaksi/store','TransaksiController@store');
Route::get('/transaksi/delete','TransaksiController@destroy');